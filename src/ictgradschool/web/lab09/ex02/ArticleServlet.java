package ictgradschool.web.lab09.ex02;

import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // request.getParameter accesses parameters from the submitted form
        String atname = request.getParameter("atname");

        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();

        out.println("<p><h1><strong>"+ atname + "</strong></h1></p>");

        String aaname = request.getParameter("aaname");
        out.println("<p> by"+" " +aaname + "</p>");

        String genreChoice=request.getParameter("choice");
        out.println("<p>Genre:"+" "+genreChoice+"</p>");




    }
}
