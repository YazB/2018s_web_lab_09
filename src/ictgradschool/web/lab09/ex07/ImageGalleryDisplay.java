package ictgradschool.web.lab09.ex07;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class ImageGalleryDisplay extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private File getFullSizeImage(String iName, File[] photoList) {
        String fullSizeImgname = iName.split("_thumbnail.png")[0];
        File fullSizeImage = null;
        for (int i = 0; i < photoList.length; i++) {
            if (photoList[i].getName().contains(fullSizeImgname + ".")) {
                fullSizeImage = photoList[i];
                return fullSizeImage;
            }
        }
        return fullSizeImage;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Use the ServetContext object to locate where on the local filesystem the /Photos directory can be found.
        PrintWriter PrintWriterout = response.getWriter();
        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");

        File pFolder = new File(fullPhotoPath);
        File[] photoList = pFolder.listFiles();


        System.out.println("<body>");
        if (photoList.length < 1) {
            PrintWriterout.print("ERROR: No cute animal pictures to be found. Please give your resident coder more coffee");
        } else {
            for (int i = 0; i < photoList.length; i++) {
                String pName = photoList[i].getName();
                if (pName.contains("_thumbnail.png")) {
                    File fullSizeImage = getFullSizeImage(pName, photoList);
                    PrintWriterout.print("<a href='../Photos/" + fullSizeImage.getName() + "'>");
                    PrintWriterout.print("<img src='../Photos/" + photoList[i].getName() + "'>");
                    PrintWriterout.print("</a>");
                    PrintWriterout.print(" " + fullSizeImage.getName());
                    PrintWriterout.print(" " + fullSizeImage.length() + "bytes");
                }
            }
        }
        PrintWriterout.print("</body>");
    }
}
