package ictgradschool.web.lab09.ex04;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServletFour extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        // request.getParameter accesses parameters from the submitted form
        String atname = request.getParameter("atname");

        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();

        out.println("<p><h1><strong>"+ atname + "</strong></h1></p>");

        String aaname = request.getParameter("aaname");
        out.println("<p> by"+" " +aaname + "</p>");


        String genreChoice=request.getParameter("choice");
        out.println("<p>Genre:"+" "+genreChoice+"</p>");


        String content = request.getParameter("atname");
        out.println("<p>This is an article about<br>"+atname+"</p>");



        String[] exerciseFour = new String[4];
        exerciseFour[0]="four";
        exerciseFour[1]="three";
        exerciseFour[2]="two";
        exerciseFour[3]="one";


        for (int i = 0; i < exerciseFour.length; i++) {
        out.println("<li>"+exerciseFour[i]+"</li>");
        }

    }
}
